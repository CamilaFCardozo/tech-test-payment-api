using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymenteApi.Models
{
    public class Vendedores
    {
        public Vendedores(string nome, string cpf, string telefone)
        {
            Nome = nome;
            Cpf = cpf;
            Telefone = telefone;
        }
        public int Id {get; set;}
        public string Nome {get; set;}
        public string Cpf {get; set;}
        public string Telefone {get; set;}

    }
}