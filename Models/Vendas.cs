using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymenteApi.Models
{
    
    public class Vendas
    {
       
        public int Id { get; set; }
        public DateTime DtVenda{ get; set; }
        public int VendedorId { get; set; }
        public string Itens { get; set; }
        public string StatusVendaDisponiveis { get; set; }
        public string StatusVenda { get; set; }
        public DateTime DtAtualizacao { get; set; }        
    }
}