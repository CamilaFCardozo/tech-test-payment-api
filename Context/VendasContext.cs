using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechTestPaymenteApi.Models;

namespace TechTestPaymenteApi.Context
{
    public class VendasContext : DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options) : base(options)
        {
            
        }
 
        public DbSet<Vendas>TbVendas{get; set;}
        public DbSet<Vendedores>TbVendedores{get; set;}
    }
}


      