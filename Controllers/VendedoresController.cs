using TechTestPaymenteApi.Models;
using TechTestPaymenteApi.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace TechTestPaymenteApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VendedoresController : ControllerBase
    {
        private readonly VendasContext _context;

        public VendedoresController(VendasContext context)
        {
            _context = context;
        }

        [HttpGet("MostrarTudo")]
        public async Task<ActionResult<IEnumerable<Vendedores>>> GetTbVendedores()
        {
            return await _context.TbVendedores.ToListAsync();
        }

        [HttpGet("SelecionarVendedorPor{id}")]
        public async Task<ActionResult<Vendedores>> GetVendedor(int id)
        {
            var vendedor = await _context.TbVendedores.FindAsync(id);

            if (vendedor == null)
            {
                return NotFound();
            }

            return vendedor;
        }

        [HttpPost("AddVendedor")]
        public async Task<ActionResult<Vendedores>> PostVendedor(Vendedores vendedor, string nome, string cpf, string telefone)
        {
            if(nome==null) return BadRequest("Informe o nome do vendedor!");
            if(cpf==null) return BadRequest("Informe o CPF do vendedor!");
            if(telefone==null) return BadRequest("Informe o telefone!");
            vendedor.Cpf=cpf;
            vendedor.Nome=nome;
            vendedor.Telefone=telefone;

            try{
                _context.TbVendedores.Add(vendedor);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetVendedor", new { id = vendedor.Id }, vendedor);

            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(ex.ToString());
            }    
        }

        private bool VendedorExists(int id)
        {
            return _context.TbVendedores.Any(e => e.Id == id);
        }
    }
}