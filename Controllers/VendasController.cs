using TechTestPaymenteApi.Models;
using TechTestPaymenteApi.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace TechTestPaymenteApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendasController(VendasContext context)
        {
            _context = context;
        }
        // Mostrar Tudo
        [HttpGet("MostrarTudo")]
         public async Task<ActionResult<IEnumerable<Vendas>>> GetTbVendas()
        {
            return await _context.TbVendas.ToListAsync();
        }

        // Obter Por ID
        [HttpGet("ObterPor{id}")]
        public async Task<ActionResult<Vendas>> GetTbVendas(int id)
        {
            var venda = await _context.TbVendas.FindAsync(id);
            if (venda == null)
            {
                return NotFound();
            }
            return venda;
        }

        //Alterar Status
        [HttpPut("AlterarStatus{id}")]
        public async Task<IActionResult> AlterarStatusVenda(int id, string status, Vendas vendas)
        {
            if(id==0)
                return BadRequest("Informe o código ID para realizar a buscar da venda");
            if(status==null)
                return BadRequest("Atualize o status da venda!");
            string[] status1=new string[2];

            //Busca Venda
            Vendas vendas1 = await _context.TbVendas.FindAsync(id);

            //Se os status for da venda for ENVIADO PARA A TRANSPORTADORA ou CANCELADO não será realizada atualização do status.
            if(vendas1.StatusVendaDisponiveis.ToUpper() == "Enviado para a Transportadora" || vendas1.StatusVendaDisponiveis.ToUpper() == "CANCELADO")
            {
                return BadRequest($"A venda NÃO aceita modificações de status: {vendas1.StatusVenda}");
            }
            status1 = vendas1.StatusVendaDisponiveis.Split(",");    
            status1[0].Trim();
            status1[1].Trim();             

            if(status.ToUpper() != status1[0].ToUpper() && status.ToUpper() != status1[1].ToUpper()  )
            {
                 return BadRequest($"Para a venda é permitido apenas um dos status: {status1[0]} ou {status1[1]}");
            }
                DateTime data = DateTime.Now;
                data.ToShortDateString();
                vendas1.StatusVenda=status;
                vendas1.DtAtualizacao=data;

                if(status.ToUpper() == "Pagamento Aprovado")
                {
                    vendas1.StatusVendaDisponiveis= "Enviado para Transportadora ou Cancelada";
                }
                else if (status.ToUpper() == "Enviado para a Transportadora")
                {
                    vendas1.StatusVendaDisponiveis="ENTREGUE";
                }

               // _context.Entry(vendas.StatusVenda).State = EntityState.Modified;            
                  vendas1.StatusVenda =  status;
                        
                try
                {
                    _context.TbVendas.Update(vendas1);
                    await _context.SaveChangesAsync();
                   // return CreatedAtAction("GetVenda", new { id = vendas.Id }, vendas);
                    return Ok();                    

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!VendaExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        return BadRequest(ex.ToString());
                    }
                }
        
        }
        

        [HttpPost("AddVenda")]
        public async Task<ActionResult<Vendas>> PostVendas(Vendas vendas, int vendedorId, string itens)
        {
            if(vendedorId==0)
            {
                return BadRequest("Informe o código de identificação do vendedor!");
            }

            DateTime data = DateTime.Now;
            data.ToShortDateString();
            vendas.VendedorId = vendedorId;
            vendas.StatusVenda = "Aguardando Pagamento";
            vendas.StatusVendaDisponiveis = "Pagamento Aprovado ou Cancelada";
            vendas.DtVenda=data;
            vendas.DtAtualizacao= data;
            vendas.Itens = itens;

            try
            {
                _context.TbVendas.Add(vendas);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetVenda", new { id = vendas.Id }, vendas);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(ex.ToString());
            }           
        }
        private bool VendaExists(int id)
        {
            return _context.TbVendas.Any(e => e.Id == id);
        }        
    }
}